!// ================================================================================
!// Copyright Steven RENDON RESTREPO - 2022-2023                                  
!// e-mail:   steven.rendon-restrepo@lam.fr                                      
!// address:  Groupe Systemes planetaires (GSP)              
!//           Laboratoire d'Astrophysique de Marseille                          
!//           Pole de l'Etoile, site de Chateau-Gombert                         
!//           38, rue Frederic Joliot-Curie                                     
!//           13388 Marseille cedex 13 France                                   
!//           CNRS U.M.R 7326                                                   
!// ================================================================================
!// See the complete license in LICENSE and/or "http://www.cecill.info".        
!// ================================================================================

!***********************************************************************************
!**          RoSSBi3D : Rotating Systems Simulation Code for Bi-fluids            **
!***********************************************************************************

!***********************************************************************************
!        Write_input_parameters.F90:  Write Initial parameters for the rossbi
!                                     analysis code
!***********************************************************************************

#include "choice.def"
subroutine Write_input_parameters

  use, intrinsic :: iso_c_binding
  use mpi
!**********************************************************************************************************

    use Variables,  only : stokes_parameter_0, aerodynamic_regime, x
    use Variables,  only : myid
    use Variables,  only : mass_disk_g_0, dy
    use Parameters, only : rin, rout, r_0, selfref, pi, gamma
    use Parameters, only : xref, vref, sigref, time_ref, press_ref, temp_ref 
    use Parameters, only : mean_free_path_ref, sigma_r0
    use Parameters, only : b_H, kb, masm, muni 
    use Parameters, only : period_r0, H_by_r0
    use Parameters, only : b_rho, b_P
    use Parameters, only : pdia, H_d
    use Parameters, only : nx_global, ny_global
    use Parameters, only : nx, ny
    use Variables, only  : p_g_0_x0_z0, rho_g_0_x0_z0, vy_g_0_x0_z0
#ifdef Z_DIMENSION
    use Parameters, only : Hmax
    use Parameters, only : nz, nz_global
    use Variables, only  : dz
    use Variables, only  : time_sedimentation
#endif

#ifdef LOG_MESH
    use Variables, only : ratio
#endif

#ifdef BOX
    use Parameters, only : box_size
#endif


#ifdef PARTICLES
    use Variables, only : mass_disk_p_0
#endif


#ifdef MODEL
      use Parameters, only : model_type_g
      use Variables, only  : rossb, wr, aspect, ampf
      use Variables, only  : mass_disk_g_bump
#ifdef PARTICLES
      use Parameters, only : model_type_p
      use Variables, only  : ampdust, wrd, aspdust
      use Variables, only  : mass_disk_p_bump
#endif
#endif



    integer :: ierr
    character(len=30)::fmt1,fmt2, fmt3, fmt4
    real(8) :: vy_g_0, cs_0
    real(8) :: temp_g_r0, Mach_r0, Q_r0
    real(8) :: mean_free_path_r0
    real(8) :: p_g_1AU, rho_g_1AU, temp_g_1AU, cs_g_1AU, Mach_1AU 
    real(8) :: Q_1AU, mean_free_path_1AU
    real(8) :: Q_c
    
    fmt1='(1A29)'
    fmt2='(1A34,1e20.10)' 
    fmt3='(1A34,1I20)'
    fmt4='(1A34, 1A20)'

    ! 1 AU Variables
    rho_g_1AU          = rho_g_0_x0_z0 / r_0**b_rho
    p_g_1AU            = p_g_0_x0_z0   / r_0**b_P
    temp_g_1AU         = p_g_1AU / rho_g_1AU
    cs_g_1AU           = dsqrt(gamma * p_g_1AU/rho_g_1AU) 
    Mach_1AU           = 1.D0 / cs_g_1AU
#ifndef Z_DIMENSION
    Q_1AU              = 1.D0 / ( Mach_1AU * selfref * pi * 1.D0**2.D0 * rho_g_1AU)
    mean_free_path_1AU = mean_free_path_ref * dsqrt(2/gamma) * 1.D0 / Mach_1AU / rho_g_1AU 
#else
    Q_1AU              = dsqrt(gamma/2.D0) / ( selfref * pi * sqrt(pi) * 1.D0**3.D0 * rho_g_1AU )
    mean_free_path_1AU = mean_free_path_ref / rho_g_1AU
#endif

    ! r_0 Variables
    vy_g_0             = vy_g_0_x0_z0
    cs_0               = dsqrt(gamma*p_g_0_x0_z0/rho_g_0_x0_z0)
    temp_g_r0          = p_g_0_x0_z0 / rho_g_0_x0_z0 
    Mach_r0            = vy_g_0 / cs_0
#ifndef Z_DIMENSION
    Q_r0               = 1.D0 / ( Mach_r0 * selfref * pi * r_0**2.D0 * rho_g_0_x0_z0 )
    mean_free_path_r0  = mean_free_path_ref * dsqrt(2/gamma) * r_0 / Mach_r0 / rho_g_0_x0_z0
#else
    Q_r0               = dsqrt(gamma/2.D0) / ( selfref * pi * sqrt(pi) * r_0**3.D0 * rho_g_0_x0_z0 )
    mean_free_path_r0  = mean_free_path_ref / rho_g_0_x0_z0
#endif

   Q_c = 1.D0/H_by_r0 
    

if (myid == 0) then
  
      call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr)
  
      open(50, file='Input_variables.data', form="formatted") !Replace Input_variables.data if exists 
       
      
      write(50,*) "*********** Simulation Parameters ***********"
      write(50,*) ""
#ifdef ONEPHASE
        write(50,fmt=fmt1) "ONEPHASE                    "
#else
        write(50,fmt=fmt1) "PARTICLES                   "
#endif
        
#ifdef THERMAL_RELAX
        write(50,fmt=fmt1) "THERMAL_RELAX               "
#else
        write(50,fmt=fmt1) "NO_THERMAL_RELAX            "
#endif
        
#ifdef INSTABILITY
        write(50,fmt=fmt1) "INSTABILITY                 "
#else
        write(50,fmt=fmt1) "MODEL                       "
#endif
        
#ifdef ISOTHERMAL
        write(50,fmt=fmt1) "ISOTHERMAL                  "
#else
        write(50,fmt=fmt1) "NO_ISOTHERMAL               "
#endif
        
#ifdef GRAVITY
        write(50,fmt=fmt1) "GRAVITY                     "
#else
        write(50,fmt=fmt1) "NO_GRAVITY                  "
#endif
        
#ifdef PLANET
        write(50,fmt=fmt1) "PLANET                      "
#else
        write(50,fmt=fmt1) "NO_PLANET                   "
#endif
        
#ifdef MIGRATION
        write(50,fmt=fmt1) "MIGRATION                   "
#else
        write(50,fmt=fmt1) "NO_MIGRATION                "
#endif
        
#ifdef SELF_GRAVITY_GAS
        write(50,fmt=fmt1) "SELF_GRAVITY_GAS            "
#else
        write(50,fmt=fmt1) "NO_SELF_GRAVITY_GAS         "
#endif

#ifdef INDUCED_GRAVITY_GAS
        write(50,fmt=fmt1) "INDUCED_GRAVITY_GAS         "
#else
        write(50,fmt=fmt1) "NO_INDUCED_GRAVITY_GAS      "
#endif

#ifdef SELF_GRAVITY_PARTICLES
        write(50,fmt=fmt1) "SELF_GRAVITY_PARTICLES      "
#else
        write(50,fmt=fmt1) "NO_SELF_GRAVITY_PARTICLES   "
#endif

#ifdef INDUCED_GRAVITY_PARTICLES
        write(50,fmt=fmt1) "INDUCED_GRAVITY_PARTICLES   "
#else
        write(50,fmt=fmt1) "NO_INDUCED_GRAVITY_PARTICLES"
#endif

#ifdef BOX
        write(50,fmt=fmt1) "BOX                         "
#else
        write(50,fmt=fmt1) "NO_BOX                      "
#endif

#ifdef FRICTION
        write(50,fmt=fmt1) "FRICTION                    "
#else
        write(50,fmt=fmt1) "NO_FRICTION                 "
#endif

#ifdef RETROACTION
        write(50,fmt=fmt1) "RETROACTION                 "
#else
        write(50,fmt=fmt1) "NO_RETROACTION              "
#endif

#ifdef Z_DIMENSION
        write(50,fmt=fmt1) "Z_DIMENSION                 "
#else
        write(50,fmt=fmt1) "NO_Z_DIMENSION              "
#endif

#ifdef REFLECTIVE_BOUNDARY
        write(50,fmt=fmt1) "REFLECTIVE_BOUNDARY         "
#else
        write(50,fmt=fmt1) "NO_REFLECTIVE_BOUNDARY      "
#endif

      
      
      write(50,*) ""
      write(50,*) "*********** Simulation Variables ***********"
      write(50,*) ""
      write(50,*) "***** Reference Physical Quantities  *******"
      write(50,*) "******* for nondimenzionalisation  *********"
      write(50,*) ""
      write(50,fmt=fmt2) "x_dim (cm)                 ", xref
      write(50,fmt=fmt2) "v_dim(cm/s)                ", vref
      write(50,fmt=fmt2) "time_dim (s)               ", time_ref
#ifndef Z_DIMENSION
      write(50,fmt=fmt2) "sigref (in g/cm**2)        ", sigref
#else
      write(50,fmt=fmt2) "sigref (in g/cm**3)        ", sigref
#endif
      write(50,fmt=fmt2) "pressure_ref (Pa)          ", press_ref * 0.1D0     ! 1 barye = 0.1 Pa
      write(50,fmt=fmt2) "temperature_ref  (K)       ", temp_ref
      write(50,fmt=fmt2) "energy_ref (erg)           ", vref**2.D0
      write(50,fmt=fmt2) "selfref                    ", selfref
      write(50,fmt=fmt2) "mean free path ref (cm)    ", mean_free_path_ref
      write(50,*) ""
      write(50,*) "************* Values at 1AU ****************"
      write(50,*) ""
      ! Here we divided by r0**b in order to recover values at 1 AU
      write(50,fmt=fmt2) "v_g (cm/s)                 ", vref 
#ifndef Z_DIMENSION
      write(50,fmt=fmt2) "sigma_g (g/cm**2)          ", sigref    * rho_g_1AU
      write(50,fmt=fmt2) "pressure (Pa/cm)           ", press_ref * 0.1D0 * p_g_1AU ! 1 barye = 0.1 Pa 
#else
      write(50,fmt=fmt2) "rho_g (g/cm**3)            ", sigref    * rho_g_1AU
      write(50,fmt=fmt2) "pressure (Pa)              ", press_ref * 0.1D0 * p_g_1AU  
#endif
      write(50,fmt=fmt2) "temperature (K)            ", temp_ref  * temp_g_1AU 
      write(50,fmt=fmt2) "sound speed (cm/s)         ", vref      * cs_g_1AU
      write(50,fmt=fmt2) "Mach number                ", Mach_1AU
      write(50,fmt=fmt2) "Ratio H/r                  ", dsqrt(2.D0 / gamma) / Mach_1AU
      write(50,fmt=fmt2) "Q (no dimension )          ", Q_1AU 
#ifndef Z_DIMENSION
      write(50,fmt=fmt2) "b_Q                        ", b_H-3.D0-b_rho
#else
      write(50,fmt=fmt2) "b_Q                        ", -3.D0-b_rho
#endif
      write(50,fmt=fmt2) "Mean free path (cm)        ", mean_free_path_1AU
      write(50,*) ""
      write(50,*) "************** Values at r0 ****************"
      write(50,*) ""
      write(50,fmt=fmt2) "v_g (cm/s)                 ", vref * vy_g_0 
#ifndef Z_DIMENSION
      write(50,fmt=fmt2) "sigma_g (g/cm**2)          ", sigref    * rho_g_0_x0_z0 
      write(50,fmt=fmt2) "pressure (Pa/cm)           ", press_ref * 0.1D0 * p_g_0_x0_z0 ! 1 barye = 0.1 Pa 
#else
      write(50,fmt=fmt2) "rho_g (g/cm**3)            ", sigref    * rho_g_0_x0_z0  
      write(50,fmt=fmt2) "pressure (Pa)              ", press_ref * 0.1D0 * p_g_0_x0_z0  
#endif
      write(50,fmt=fmt2) "temperature (K)            ", temp_ref  * temp_g_r0 
      write(50,fmt=fmt2) "sound speed (cm/s)         ", vref * cs_0 
      write(50,fmt=fmt2) "Mach number (no dimension) ", Mach_r0
      write(50,fmt=fmt2) "Ratio H/r (no dimension)   ", dsqrt(2.D0 / gamma) / Mach_r0
      write(50,fmt=fmt2) "power law H : b_H          ", b_H
      write(50,fmt=fmt2) "Q (no dimension)           ", Q_r0
#ifndef Z_DIMENSION
      write(50,fmt=fmt2) "b_Q                        ", b_H-3.D0-b_rho
#else
      write(50,fmt=fmt2) "b_Q                        ", -3.D0-b_rho
#endif
      write(50,fmt=fmt2) "Mean free path (cm)        ", mean_free_path_r0
      write(50,fmt=fmt4) "aerodynamic regime         ", aerodynamic_regime
      write(50,fmt=fmt2) "St = Omega_K*T_St =        ", stokes_parameter_0
#ifdef Z_DIMENSION
      write(50,fmt=fmt2) "time_sedimentation/period_r0", time_sedimentation/period_r0
#endif
      write(50,*) ""
      write(50,*) "***************** Mesh *********************"
      write(50,*) ""
      write(50,fmt=fmt3) "nx_global                  ", nx_global
      write(50,fmt=fmt3) "ny_global                  ", ny_global
#ifndef Z_DIMENSION
      write(50,fmt=fmt3) "Cells/processor            ", nx*ny
#else
      write(50,fmt=fmt3) "nz_global                  ", nz_global
      write(50,fmt=fmt3) "Cells/processor            ", nx*ny*nz
#endif
#ifdef LIN_MESH
      write(50,fmt=fmt2) "x resolution (in AU)       ", (rout-rin)/nx_global
#endif
#ifdef LOG_MESH
      write(50,fmt=fmt2) "x resolution (in AU)       ", r_0*(dsqrt(ratio)-1.D0*dsqrt(ratio))
#endif
      write(50,fmt=fmt2) "y resolution (in AU)       ", r_0*dy
#ifdef Z_DIMENSION
      write(50,fmt=fmt2) "z resolution (in AU)       ", dz
#endif
      write(50,fmt=fmt2) "x resolution (cells per H) ", nx_global*H_by_r0*r_0/(rout-rin)
      write(50,fmt=fmt2) "y resolution (cells per H) ", H_by_r0*r_0/r_0/dy
#ifdef Z_DIMENSION
      write(50,fmt=fmt2) "z resolution (cells per H) ", H_by_r0*r_0/dz
#endif
#ifdef BOX
      write(50,fmt=fmt2) "Box_size                   ", box_size
#endif
      write(50,*) ""
      write(50,*) "************* Disk parameters **************"
      write(50,*) ""
      write(50,fmt=fmt2) "rin (A.U)                  ", rin
      write(50,fmt=fmt2) "rout(A.U)                  ", rout
      write(50,fmt=fmt2) "r_0 (A.U)                  ", r_0
      write(50,fmt=fmt2) "H(r=r_0)(A.U)              ", H_by_r0*r_0
      write(50,fmt=fmt2) "H_d(r_0)/H_g(r_0)          ", H_d/H_by_r0/r_0
      write(50,fmt=fmt2) "period_r0 (years)          ", period_r0/2D0/pi
#ifdef Z_DIMENSION
      write(50,fmt=fmt2) "Disk height (in H_g)       ", Hmax/H_by_r0/r_0
#endif
      write(50,*) ""
      write(50,*) "   ******** Computed quantities ********    "
      write(50,*) ""
      write(50,*) "***** Critical Toomre parameter in r0 ******"
      write(50,*) ""
      write(50,fmt=fmt2) "Q_c = 1/(H/r) =            ", Q_c
      write(50,*) ""
      write(50,fmt=fmt2) "Q = Q_c/4  if sigma_r0 =   ", sigma_r0*Q_r0/(Q_c/4.D0)
      write(50,fmt=fmt2) "Q = Q_c/3  if sigma_r0 =   ", sigma_r0*Q_r0/(Q_c/3.D0)
      write(50,fmt=fmt2) "Q = Q_c/2  if sigma_r0 =   ", sigma_r0*Q_r0/(Q_c/2.D0)
      write(50,fmt=fmt2) "Q = Q_c/1.5  if sigma_r0 = ", sigma_r0*Q_r0/(Q_c/1.5D0)
      write(50,fmt=fmt2) "Q = Q_c if sigma_r0 =      ", sigma_r0*Q_r0/(Q_c) 
      write(50,fmt=fmt2) "Q = 2*Q_c  if sigma_r0 =   ", sigma_r0*Q_r0/(2*Q_c)
      write(50,fmt=fmt2) "Q = 4*Q_c  if sigma_r0 =   ", sigma_r0*Q_r0/(4*Q_c)
      write(50,*) ""
      write(50,*) "********** Toomre parameter in r0 **********"
      write(50,*) ""
      write(50,fmt=fmt2) "Q = 1   if sigma_r0 =      ", sigma_r0*Q_r0/1.D0 
      write(50,fmt=fmt2) "Q = 3   if sigma_r0 =      ", sigma_r0*Q_r0/3.D0
      write(50,fmt=fmt2) "Q = 5   if sigma_r0 =      ", sigma_r0*Q_r0/5.D0
      write(50,fmt=fmt2) "Q = 10  if sigma_r0 =      ", sigma_r0*Q_r0/10.D0
      write(50,fmt=fmt2) "Q = 20  if sigma_r0 =      ", sigma_r0*Q_r0/20.D0
      write(50,fmt=fmt2) "Q = 30  if sigma_r0 =      ", sigma_r0*Q_r0/30.D0
      write(50,fmt=fmt2) "Q = 50  if sigma_r0 =      ", sigma_r0*Q_r0/50.D0
      write(50,fmt=fmt2) "Q = 100 if sigma_r0 =      ", sigma_r0*Q_r0/100.D0
      write(50,*) ""
      write(50,*) "****** Friction parameters in r0, for ******"
      write(50,*) "****** pdia =", pdia, "(cm) *"
      write(50,*) ""
      write(50,fmt=fmt2) "St = 0.01 if pdia =        ", pdia*0.01D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 0.1  if pdia =        ", pdia*0.1D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 0.5  if pdia =        ", pdia*0.5D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 1    if pdia =        ", pdia*1.D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 5    if pdia =        ", pdia*5.D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 10   if pdia =        ", pdia*10.D0/stokes_parameter_0 
      write(50,fmt=fmt2) "St = 100  if pdia =        ", pdia*100.D0/stokes_parameter_0 
      write(50,*) ""
      write(50,*) ""
      write(50,*) "****************** Gas *********************"
      write(50,*) ""
      write(50,fmt=fmt2) "mass_disk_g_0/m_sun        ", mass_disk_g_0*selfref

#ifdef INSTABILITY      
      write(50,*) ""
      write(50,*) "      ****** RWI Instability ******"
      write(50,*) ""
#endif

#ifdef MODEL
      write(50,*) ""
      write(50,*) "      *********** Model ***********"
      write(50,*) ""
      write(50,fmt=fmt4) "model type for gas         ", model_type_g
      write(50,fmt=fmt2) "rossb                      ", rossb
      write(50,fmt=fmt2) "wr                         ", wr
      write(50,fmt=fmt2) "aspect                     ", aspect
      write(50,fmt=fmt2) "ampf_0                     ", ampf
      write(50,fmt=fmt2) "mass_disk_g_bump/m_sun     ", mass_disk_g_bump*selfref
#endif
      
      write(50,*) ""
      write(50,*) ""
      
#ifdef PARTICLES
      write(50,*) "*************** Particles ******************"
      write(50,*) ""
      write(50,fmt=fmt2) "mass_disk_p_0/m_sun        ", mass_disk_p_0*selfref

#ifdef MODEL
      write(50,*) ""
      write(50,*) "      *********** Model ***********"
      write(50,*) ""
      write(50,fmt=fmt4) "model type for particles   ", model_type_p
      if (model_type_p /= 'uniform') then
          write(50,fmt=fmt2) "wrd                    ", wrd
          write(50,fmt=fmt2) "aspdust                ", aspdust
          write(50,fmt=fmt2) "ampdust                ", ampdust
          write(50,fmt=fmt2) "mass_disk_p_bump/m_sun ", mass_disk_p_bump*selfref
      endif
#endif
      write(50,*) ""
      write(50,*) ""
#endif      
      
#if defined SELF_GRAVITY_GAS || defined SELF_GRAVITY_PARTICLES
      write(50,*) ""
      write(50,*) "************** Self gravity ****************"
      write(50,*) ""
      write(50,*) ""
      write(50,*) ""
     
#endif

      close(50)


endif
  
end subroutine Write_input_parameters


