# RoSSBi3D : Rotating Systems Simulation code for Bi-fluids at 3D

The RoSSBi3D (Rotating Systems Simulation Code for Bi-fluids) code is an open source 
numerical code specificcally designed for protoplanetary discs study at 2D and 3D. It is a 
finite volume code which is second order in time, accounts for discontinuities thanks to an 
exact Riemann solver and features self-gravity (2D). This FORTRAN 90 code solves the fully 
compressible inviscid Euler, continuity and energy conservation equations in polar coordinates
for an ideal gas orbiting a central object. Solid particles are treated as a pressureless 
fluid and interact with the gas through aerodynamic forces. 
The code works on high performance computers thanks to the MPI standard (CPU).

## Proper reference

When the RoSSBi3D code, or parts of it, are used for a scientific work, publication or 
communications the following reference should be cited:
[RoSSBi3D](https://ui.adsabs.harvard.edu/abs/2022arXiv220704252R/abstract)

## User's guide

The user's guide can be found in the [wiki](https://gitlab.lam.fr/srendon/rossbi3d/-/wikis/home). 

## Required libraries and download

Required libraries: HDF5, FFTW3

```
git clone git@gitlab.lam.fr:srendon/rossbi3d.git rossbi3d
```

## Options available

2D: Self-gravity, indirect gravity, dust drag, dust back-reaction, planet

3D: indirect gravity, dust drag, dust back-reaction

The 2D self-gravity calculation is based in the smoothing length formalism and
its correction brought by [Smoothing length article ](https://arxiv.org/abs/2303.16213)

## First run 

```
cd rossbi3d/src
make 
cd rossbi3d
run src/run.out > screen_out
```

## License 

RoSSBi3D is governed by the CeCILL2  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info/index.en.html" and/or LICENSE file.      

## Copyright

                         --  RoSSBi3D --

            Copyright ©Steven RENDON RESTREPO 2022-2023
               git@gitlab.lam.fr:srendon/rossbi3d.git

                   steven.rendon-restrepo_AT_lam_DOT_fr
                     Groupe Systemes Planetaires (GSP)
              Laboratoire d'Astrophysique de Marseille
                         (CNRS U.M.R 7326)
             Pôle de l Etoile Site de Château-Gombert
                    38, rue Frédéric Joliot-Curie
                      13388 Marseille cedex 13
                              FRANCE


## Authors

3D version development      : Steven Rendon Restrepo                          
2D and 3D MPI optimisation  : Steven Rendon Restrepo and Radim Vavrik   
2D self-gravity optimisation: Steven Rendon Restrepo   
Git deployment, mantainer   : Steven Rendon Restrepo   
2D version development      : Clement SURVILLE   
Riemann solver              : Satoshi INABA, Pierre BARGE,   
                              Eric DANIEL & Herve GUILLARD                                            

## Next steps and possible development

Viscosity (2D and 3D), self-gravity (3D), Radiative transfer pipeline

