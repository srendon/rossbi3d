#!/bin/bash
#SBATCH -J Job_name
#SBATCH --nodes 10
#SBATCH --ntasks-per-node=12
#SBATCH --hint=nomultithread
#SBATCH -t 0-00:40:00
##SBATCH --mem=2GB               ## Mem is not advised for JZ cluster
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
##SBATCH --mail-user=steven.rendon-restrepo@lam.fr

### Working Folder
# Jobs should be launched from $WORK

### Interactive jobs
### srun --pty --ntasks=1 --cpus-per-task=4 --hint=nomultithread --time=1:30:0  bash

### Submit a job
### sbatch submit.sh

ulimit -s unlimited

module purge
module load intel-compilers/19.0.4
module load intel-mpi/2019.4
module load fftw/3.3.8-mpi
module load hdf5/1.12.0-mpi

srun src/run.out > screen_out

