#!/bin/bash
#MSUB -r Job_Name           # Request name
#MSUB -c 48                 # Number of cores
#MSUB -N 3                  # Number of nodes
#MSUB -T 600                # Elapsed time limit in seconds of the job
#MSUB -o ./%I.msub.output   # Standard output. %I is the job id
#MSUB -e ./%I.msub.err      # Error output. %I is the job id
#MSUB -q skylake            # We submit the job in a skylake node
#MSUB -A gen11982           # Project
#MSUB -m scratch,work,store

cd ${BRIDGE_MSUB_PWD}

### Interactive jobs
### ccc_mprun -p skylake -c 16 -s

### Submit a job
### ccc_msub submit.sh 

ulimit -s unlimited

module purge
module load flavor/buildcompiler/intel/20 flavor/buildmpi/openmpi/4.0
module load feature/mkl/multi-threaded intel/20.0.0 mpi/openmpi/4.0.3
module load flavor/hdf5/parallel hdf5/
module load fftw3/mkl/20.0.0


ccc_mprun src/run.out > screen_out

#srun --mpi=pmi2 -K1 --resv-ports -n $SLURM_NTASKS code/run.out > screen_out


