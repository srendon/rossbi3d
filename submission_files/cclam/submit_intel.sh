#!/bin/bash
#SBATCH -J test
#SBATCH --nodes 4
#SBATCH --ntasks-per-node=32                    
#SBATCH --cpus-per-task=1 # 1 cpu per  task
#SBATCH -t 0-02:30:00
##SBATCH --mem=24GB               ## Memory per node. Usually a calculation requires maximum 0.1Gb/processo
#SBATCH --begin=now
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

## In cclam it is important it is not nomultithread

### Interactibe Jobs
# (Short partition)
### srun -N1 -c12 -t 01:00:00 -p short --mem 36GB --x11 --pty bash
# (Memory partition)
### srun -N1 -c4 -p mem -t 03:00:00 --mem 256GB --pty bash
# (Batch partition)
### srun -N1 -c24 -p batch -t 03:00:00 --mem 60GB --pty bash

### Submit a job
### sbatch -p mpi submit.sh

ulimit -s unlimited

module purge
module load compiler/2022.0.2
module load mpi/2021.5.1
module load hdf5/1.12.2/intel/2022.0.2
module load fftw/3.3.9/intel/2022.0.2

## !!!! MANDATORY
export PSM_SHAREDCONTEXTS_MAX=8

mpirun src/run.out > screen_out


