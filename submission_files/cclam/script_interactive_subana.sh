#!/bin/bash

### Parameters for Ray (parallel)
let "worker_num=(${SLURM_NNODES} - 1)"

# Define the total number of CPU cores available to ray
let "total_cores=${worker_num} * ${SLURM_CPUS_ON_NODE}"

nodes=$(scontrol show hostnames $SLURM_JOB_NODELIST) # Getting the node names
nodes_array=( $nodes )

head_node=${nodes_array[0]}

ip_prefix=$(srun --nodes=1 --ntasks=1 -w $head_node hostname --ip-address) # Making address
suffix=':6379'
ip_head=$ip_prefix$suffix
#redis_password='steven' ##$(uuidgen)
export ip_head # Exporting for latter access by trainer.py
echo "IP_HEAD: $ip_head"

echo "STARTING HEAD at $head_node"
srun --nodes=1 --ntasks=1 -w $head_node \
    ray start --head --node-ip-address="$ip_prefix" --port=6379 \
    --num-cpus ${SLURM_CPUS_ON_NODE} --block &
#srun --nodes=1 --ntasks=1 -w $head_node ray start --head --block --dashboard-host 0.0.0.0 --port=6379 --num-cpus=${SLURM_CPUS_ON_NODE} &
#srun --nodes=1 --ntasks=1 -w $head_node ray start --block --head --redis-address=${ip_head} --redis-password=$redis_password & # Starting the head
sleep 10
# Make sure the head successfully starts before any worker does, otherwise
# the worker will not be able to connect to redis. In case of longer delay,
# adjust the sleeptime above to ensure proper order.

node_array_for_python=${head_node}
for (( i=1; i<=$worker_num; i++ ))
do
  node_i=${nodes_array[$i]}
  echo "Starting WORKER $i at $node_i"
  node_array_for_python="${node_array_for_python} , ${nodes_array[$i]}"
  srun --nodes=1 --ntasks=1  -w $node_i \
      ray start --address=$ip_head \
      --num-cpus=${SLURM_CPUS_ON_NODE} --block & # Starting the workers
  # Flag --block will keep ray process alive on each compute node.
  sleep 5
done

export node_array_for_python
