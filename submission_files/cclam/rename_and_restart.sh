#!/bin/bash

# Script for multiple depend jobs submissions

# restart.data.h5 doens't exist and 001.data.h5 exist (after the first simulation)
# -------------------------------------------
# -> the data.XXX.h5 file with the greatest XXX number
#    is renamed to restart.data.h5
if [ -f 001.data.h5 ] && [ ! -f restart.data.h5 ]; then
  # pick up the name of the last data file
  greatest_numbered_filename=$(ls *.data.h5 | tail -1)

  # change the name of previous file to restart.data.h5
  cp "${greatest_numbered_filename}" restart.data.h5

fi

# restart.data.h5 and 001.data.h5 the exist (after many simulations)
# -----------------------------------------
# -> restart.data.h5 is erased 
# -> the data.XXX.h5 file with the greatest XXX number
#    is renamed to restart.data.h5
if [ -f 001.data.h5 ] && [ -f restart.data.h5 ]; then

  # Erase the restart file wheter it exists or not
  rm -f restart.data.h5

  # pick up the name of the last data file
  greatest_numbered_filename=$(ls *.data.h5 | tail -1)

  # change the name of previous file to restart.data.h5
  cp "${greatest_numbered_filename}" restart.data.h5
fi

# In the case were only the restart.data.h5 file exists in the folder
# nothing happens (because the simulation should start from this file)


