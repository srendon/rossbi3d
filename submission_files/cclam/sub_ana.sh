#!/bin/bash
#SBATCH -J ana
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
##SBATCH --threads-per-core=1
#SBATCH -t 0-04:00:00
#SBATCH --begin=now
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr


### Parameters for Ray (parallel)
let "worker_num=(${SLURM_NNODES} - 1)"

# Define the total number of CPU cores available to ray
let "total_cores=${worker_num} * ${SLURM_CPUS_ON_NODE}"

nodes=$(scontrol show hostnames $SLURM_JOB_NODELIST) # Getting the node names
nodes_array=( $nodes )

node1=${nodes_array[0]}

ip_prefix=$(srun --nodes=1 --ntasks=1 -w $node1 hostname --ip-address) # Making address
suffix=':6379'
ip_head=$ip_prefix$suffix
#redis_password='steven' ##$(uuidgen)

export ip_head # Exporting for latter access by trainer.py

srun --nodes=1 --ntasks=1 -w $node1 ray start --head --block --dashboard-host 0.0.0.0 --port=6379 --num-cpus ${SLURM_CPUS_ON_NODE} &
#srun --nodes=1 --ntasks=1 -w $node1 ray start --block --head --redis-address=${ip_head} --redis-password=$redis_password & # Starting the head
sleep 40
# Make sure the head successfully starts before any worker does, otherwise
# the worker will not be able to connect to redis. In case of longer delay,
# adjust the sleeptime above to ensure proper order.

node_array_for_python=${node1}
for ((  i=1; i<=$worker_num; i++ ))
do
  node2=${nodes_array[$i]}
  node_array_for_python="${node_array_for_python} , ${nodes_array[$i]}"
  srun --nodes=1 --ntasks=1  -w $node2 ray start --block --address=$ip_head --num-cpus ${SLURM_CPUS_ON_NODE} & # Starting the workers
  # Flag --block will keep ray process alive on each compute node.
  sleep 5
done

export node_array_for_python


# For new Rossbi code version (Input_parameters.data output file)
run_analysis.sh
