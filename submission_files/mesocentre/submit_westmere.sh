#!/bin/bash
#SBATCH -J XX
#SBATCH -p westmere
#SBATCH -N 10
#SBATCH --ntasks-per-node=12
##SBATCH --threads-per-core=1
#SBATCH -A westmere
#SBATCH -t 1-00:00:00
#SBATCH --begin=now
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

module purge
module load userspace/all
module load fftw3/icc18/openmpi/3.3.6-pl2
module load hdf5/icc18/openmpi/1.10.1

srun --mpi=pmi2 -K1 --resv-ports -n $SLURM_NTASKS code/run.out > screen_out


