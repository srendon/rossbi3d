#!/bin/bash
#SBATCH -J Job_name
#SBATCH -p skylake
#SBATCH -N 10
#SBATCH --ntasks-per-node=12
##SBATCH --threads-per-core=1
#SBATCH -A b072
#SBATCH -t 0-00:40:00
##SBATCH --mem=2GB               ## Memory per node. Usually a calculation requires maximum 0.1Gb/processor
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

### Working Folder
# Jobs should be launched from /scratch/username

### Interactive jobs
### srun -p skylake --time=2:30:0 -N 1 --ntasks-per-node=12 --pty bash -i

### Submit a job
### sbatch submit.sh

ulimit -s unlimited

module purge
module load userspace/all
module load intel-compiler/64/2020.2.254
module load fftw3/icc18/openmpi/3.3.6-pl2
module load hdf5/icc18/openmpi/1.10.1
module load python3/3.6.3

srun --mpi=pmi2 -K1 --resv-ports -n $SLURM_NTASKS code/run.out > screen_out


