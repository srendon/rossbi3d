#! /bin/bash

# --parsable parameter allows to return only the job id
# first job - no dependencies
jid1=$(sbatch --parsable submit_westmere.sh)

# second job depend on first job
jid2=$(sbatch --depend=afterany:$jid1 --parsable submit_westmere.sh)

# third job depend on second job
jid3=$(sbatch --depend=afterany:$jid2 --parsable submit_westmere.sh)

# show dependencies in squeue output:
squeue -u $USER -o "%.8A %.4C %.10m %.20E"

