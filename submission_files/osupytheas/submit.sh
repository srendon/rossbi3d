#!/bin/bash
#SBATCH -J Job_Name
#SBATCH -N 14
#SBATCH --ntasks-per-node=12
##SBATCH --threads-per-core=1
#SBATCH -t 2-00:00:00
#SBATCH --mem=2GB               ## Memory per node. Usually a calculation requires maximum 0.1Gb/processo
#SBATCH --begin=now
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

module load hdf5/hdf51105
FI_PROVIDER=sockets ## There is an MPI error which is corrected with this variable
export FI_PROVIDER


## !!!! MANDATORY
export PSM_SHAREDCONTEXTS_MAX=8

mpirun src/run.out > screen_out


