#!/bin/bash
#SBATCH -J ana
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
##SBATCH --threads-per-core=1
#SBATCH -t 0-04:00:00
#SBATCH --begin=now
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

source virtualenvwrapper.sh
workon mon_environnement_36

# For new Rossbi code version (Input_parameters.data output file)
run_analysis.sh
