#!/bin/bash
#SBATCH -J Job_name
#SBATCH -N 5
#SBATCH --ntasks-per-node=24
##SBATCH --threads-per-core=1
#SBATCH -t 1-00:00:00
#SBATCH -C HSW24        
#SBATCH --mem=4GB               ## Memory per node. Usually a calculation requires maximum 0.1Gb/processor
#SBATCH -o ./%N.%j.%slurm.output
#SBATCH -e ./%N.%j.%slurm.err
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=steven.rendon-restrepo@lam.fr

### Working Folder
# Jobs should be launched from $WORKDIR or $SCRATCHDIR

### Interactive jobs
### Not possible in occigen

### Submit a job
### sbatch submit.sh

ulimit -s unlimited

module purge
module load intel/19.4 intelmpi/2019.4.243
module load hdf5/1.10.5-intel-19.0.4-intelmpi-2019.4.243
module load fftw/3.3.8-intel-19.0.3-intelmpi-2019.3.199

srun --mpi=pmi2 -K1 --resv-ports -n $SLURM_NTASKS src/run.out > screen_out


